var app = angular.module('game', []);

app.controller('GameController',  ['$scope', function($scope){
    
    $scope.whoWins = function(userClick) {        
        var computerChoice = getRand();
        computerChoice = allGamePieces[computerChoice];
        var userChoice = allGamePieces[userClick];
        comparePieces(userChoice, computerChoice);
    };
    
    $scope.userScore=0;
    $scope.compScore=0;
    $scope.tieScore=0;
    
    function gamePiece(type, beats, losesTo){
        this.type = type;
        this.beats = beats;
        this.losesTo = losesTo;        
    };
    
    var rock = new gamePiece("rock", ["paper"], ["scissors", "hammer", "dynamite"]);
    var paper = new gamePiece("paper", ["scissors", "hammer"], ["rock", "dynamite"]);
    var scissors = new gamePiece("scissors", ["rock"], ["paper", "hammer", "dynamite"]);
    var hammer = new gamePiece("hammer", ["scissors", "rock"], ["paper", "dynamite"]);
    var dynamite = new gamePiece("dynamite", ["scissors", "rock", "paper", "hammer"], []);
    
    var allGamePieces = [];
    allGamePieces["rock"]=rock;
    allGamePieces["paper"]=paper;
    allGamePieces["scissors"]=scissors;
    allGamePieces["hammer"]=hammer;
    allGamePieces["dynamite"]=dynamite;
    
    function getRand() {
        var keysArray = Object.keys(allGamePieces);
        var max = keysArray.length, min = 0;
        var comPiece = Math.floor(Math.random() * (max - min));
        return keysArray[comPiece];    
    }

    function comparePieces(user, computer){
        console.log(user.type, computer.type);
        $scope.userselection= "User Choice: " + user.type;
        $scope.compselection= "Computer Choice: " + computer.type;

        if(user.type == computer.type){
            $scope.result="Nobody wins, it's a TIE !!";
            $scope.tieScore++;
        }
        else if(user.beats.includes(computer.type)){
            $scope.result="You win because " + computer.type + " loses to " + user.type;
            $scope.userScore++;
        }
        else{
            $scope.result="Computer wins because " + user.type + " loses to " + computer.type; 
            $scope.compScore++;
        }
    }    
    
}]);