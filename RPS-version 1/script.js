function gamePiece(type, beats, losesTo){
    this.type = type;
    this.beats = beats;
    this.losesTo = losesTo;
}

var rock = new gamePiece("rock", ["paper"], ["scissors", "hammer", "dynamite"]);
var paper = new gamePiece("paper", ["scissors", "hammer"], ["rock", "dynamite"]);
var scissors = new gamePiece("scissors", ["rock"], ["paper", "hammer", "dynamite"]);
var hammer = new gamePiece("hammer", ["scissors", "rock"], ["paper", "dynamite"]);
var dynamite = new gamePiece("dynamite", ["scissors", "rock", "paper", "hammer"], []);

var allGamePieces = [];

allGamePieces["rock"]=rock;
allGamePieces["paper"]=paper;
allGamePieces["scissors"]=scissors;
allGamePieces["hammer"]=hammer;
allGamePieces["dynamite"]=dynamite;

function whoWins(obj, userClick) {
    var computerChoice = getRand();
    computerChoice = allGamePieces[computerChoice];
    var userChoice = allGamePieces[userClick];
    comparePieces(userChoice, computerChoice);
};

function getRand() {
    var keysArray = Object.keys(allGamePieces);
    var max = keysArray.length, min = 0;
    var comPiece = Math.floor(Math.random() * (max - min));
    return keysArray[comPiece];    
}

function comparePieces(user, computer){
    console.log(user.type, computer.type);
    document.getElementById('choice').innerHTML ="User Choice: "+user.type.toUpperCase()+" || Computer Choice: "+computer.type.toUpperCase();
    if(user.type == computer.type)
        document.getElementById('result').innerHTML = "Nobody wins, it's a TIE !!";
    else if(user.beats.includes(computer.type))
        document.getElementById('result').innerHTML = "You win because " + computer.type + " loses to " + user.type;
    else
        document.getElementById('result').innerHTML = "Computer wins because " + user.type + " loses to " + computer.type;        
}


